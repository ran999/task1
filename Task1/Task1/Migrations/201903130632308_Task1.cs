namespace Task1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Task1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Farmacies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Dosage = c.String(),
                        Volume = c.String(),
                        Manufacturer = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Farmacies");
        }
    }
}
