﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Task1.Models
{
    public class FarmContext:DbContext
    {
        public FarmContext() : base("DefaultConnection")
        {

        }
        public DbSet<Farmacy> Farmacys { get; set; }
    }
}
