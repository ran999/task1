﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Task1.Models;
using System.Data;
using System.Xml;
using Microsoft.Win32;

namespace Task1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FarmContext db;
        public MainWindow()
        {
            InitializeComponent();

            db = new FarmContext();
            db.Farmacys.Load();
            farmGrid.ItemsSource = db.Farmacys.Local.ToBindingList();
            var listmanufact = db.Farmacys.Local.ToBindingList();
            manufactList.Items.Add("Все производители");
            foreach (var item in listmanufact)
            {
                if (!manufactList.Items.Contains(item.Manufacturer))
                {
                    manufactList.Items.Add(item.Manufacturer);
                }
            }
            this.Closing += MainWindow_Closing;
        }
        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
            db.Dispose();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            AddWindow aw = new AddWindow();


            db = new FarmContext();
            if (aw.ShowDialog() == true)
            {
                Farmacy farm = new Farmacy();
                farm.Name = aw.Name;
                farm.Dosage = aw.Dosage;
                farm.Volume = aw.Volume;
                farm.Manufacturer = aw.Manufacturer;

                var p = db.Farmacys.Where(s => s.Name.ToUpper() == aw.Name.ToUpper()).Where(s => s.Dosage.ToUpper() == aw.Dosage.ToUpper()).Where(s => s.Volume.ToUpper() == aw.Volume.ToUpper()).Where(s => s.Manufacturer.ToUpper() == aw.Manufacturer.ToUpper()).FirstOrDefault<Farmacy>();
                if (p != null)
                {
                    MessageBox.Show("Данный продукт уже существует");
                }
                else
                {
                    db.Farmacys.Add(farm);
                    db.SaveChanges();
                }
            }
            else
            {
                MessageBox.Show("Данные не добавлены");
            }

            farmGrid.ItemsSource = db.Farmacys.ToList();
            var listmanufact = db.Farmacys.Local.ToBindingList();
            manufactList.Items.Add("Все производители");
            foreach (var item in listmanufact)
            {
                if (!manufactList.Items.Contains(item.Manufacturer))
                {
                    manufactList.Items.Add(item.Manufacturer);
                }
            }
            db.Dispose();
            
        }
        private void filterButton_Click(object sender, RoutedEventArgs e)
        {
            db = new FarmContext();
            if (manufactList.SelectedItem.ToString() != "Все производители")
            {
                farmGrid.ItemsSource = db.Farmacys.Where(s => s.Manufacturer == manufactList.SelectedItem.ToString()).ToList();
                
            }
            else
            {
                farmGrid.ItemsSource = db.Farmacys.ToList();
            }
            db.SaveChanges();

        }
        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (farmGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < farmGrid.SelectedItems.Count; i++)
                {
                    Farmacy farm = farmGrid.SelectedItems[i] as Farmacy;
                    if (farm != null)
                    {
                        db.Farmacys.Remove(farm);
                    }
                }
            }
            db.SaveChanges();
        }
        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.DefaultExt = ".xml";
            string path = "";
            if (savefile.ShowDialog()==true)
            {
                path = savefile.FileName;
            }
            if (path != "")
            {
                try
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    dt.TableName = "Farmacy";
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Dosage");
                    dt.Columns.Add("Volume");
                    dt.Columns.Add("Manufacturer");
                    ds.Tables.Add(dt);

                    var listmanufact = db.Farmacys.ToList();
                    foreach (var r in listmanufact)
                    {
                        DataRow row = ds.Tables["Farmacy"].NewRow();
                        row["Name"] = r.Name;
                        row["Dosage"] = r.Dosage;
                        row["Volume"] = r.Volume;
                        row["Manufacturer"] = r.Manufacturer;
                        ds.Tables["Farmacy"].Rows.Add(row);
                    }
                    ds.WriteXml(path);
                    MessageBox.Show("XML файл успешно сохранен.", "Выполнено.");
                }
                catch
                {
                    MessageBox.Show("Невозможно сохранить XML файл.", "Ошибка.");
                }
            }
        }
        private void uploadButton_Click(object sender, RoutedEventArgs e)
        {
            db = new FarmContext();
            OpenFileDialog myDialog = new OpenFileDialog();
            string xmlpath = "";
            if (myDialog.ShowDialog() == true)
            {
                xmlpath = myDialog.FileName;
            }
            if (xmlpath != "")
            {
                XmlDocument xml = new XmlDocument();
                xml.Load(xmlpath);

                XmlElement xRoot = xml.DocumentElement;
                foreach (XmlNode xnode in xRoot)
                {
                    Farmacy farm = new Farmacy();
                    foreach (XmlNode childnode in xnode.ChildNodes)
                    {
                        if (childnode.Name == "Name")
                        {
                            farm.Name = childnode.InnerText;
                        }
                        if (childnode.Name == "Dosage")
                        {
                            farm.Dosage = childnode.InnerText;
                        }
                        if (childnode.Name == "Volume")
                        {
                            farm.Volume = childnode.InnerText;
                        }
                        if (childnode.Name == "Manufacturer")
                        {
                            farm.Manufacturer = childnode.InnerText;
                        }
                    }
                    var p = db.Farmacys.Where(s => s.Name.ToUpper() == farm.Name.ToUpper()).Where(s => s.Dosage.ToUpper() == farm.Dosage.ToUpper()).Where(s => s.Volume.ToUpper() == farm.Volume.ToUpper()).Where(s => s.Manufacturer.ToUpper() == farm.Manufacturer.ToUpper()).FirstOrDefault<Farmacy>();
                    if (p == null)
                    {
                        db.Farmacys.Add(farm);
                    }
                }
                db.SaveChanges();
                MessageBox.Show("Данные из XML загружены");
            }
            farmGrid.ItemsSource = db.Farmacys.ToList();
            var listmanufact = db.Farmacys.Local.ToBindingList();
            manufactList.Items.Add("Все производители");
            foreach (var item in listmanufact)
            {
                if (!manufactList.Items.Contains(item.Manufacturer))
                {
                    manufactList.Items.Add(item.Manufacturer);
                }
            }

            db.Dispose();
        }
    }
}
